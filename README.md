**This project's goal is to create neural networks that help design new experiments for the BESSY electron accelerator by training the networks on simulation data. In a first step, the networks are trained and then used to predict the experiment outcome based on input parameters. After that, we attempt to predict the necessary input parameters for a desired outcome. Finally, we try to optimize the networks to function with noisy data.**

**Project Data**

The training data for the neural network are simulations provided by Peter Feuer Forson.

**Usage**

This code has been tested and developed using PyCharm and Visual Studio Code and Python 3.9. 
Set hyperparameters and run the code to start training a neural network with the set parameters.
If there is already a fully trained model with the set parameters in the "Modelle" folder, the program will evaluate that model. If there is a partly trained model, it will use that as a checkpoint and continue training it. The program saves models every 10 Epochs.
It is recommended to train the models on a GPU with CUDA, as this is much faster than training them on a CPU. 

**Contributors**

- Lionel Kress
- Laurids Radtke
- Martin Kühn
- Qiqi Chen
- Michal Brus
- Clemens Paul

