import math
import pandas as pd
import numpy as np
from numpy import vstack, sqrt
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler
import torch
from torch import Tensor, tensor
from torch.nn import Linear, Sigmoid, Module, MSELoss
from torch.optim import SGD, Adam
from torch.nn.init import xavier_uniform_
from torch.utils.data import Dataset, DataLoader, random_split
import os
from tqdm import tqdm
import random
import matplotlib.pyplot as plt
import matplotlib as mpl


class CSVDataset(Dataset):
    # load the dataset
    def __init__(self, path, numFeatures, numTargets, deviceName, normalize):
        # load the csv file as a dataframe
        df = pd.read_csv(path, header=None)
        if normalize:
            df = pd.DataFrame(MinMaxScaler().fit_transform(df))
        # store the inputs and outputs
        self.X = tensor(df.values[:, :numFeatures].astype('float32')).to(device=deviceName, non_blocking=True)
        self.y = tensor(df.values[:, -numTargets:].astype('float32')).to(device=deviceName, non_blocking=True)
        # ensure target has the right shape
        self.y = self.y.reshape((len(self.y), numTargets))

    # number of rows in the dataset
    def __len__(self):
        return len(self.X)

    # get a row at an index
    def __getitem__(self, idx):
        return [self.X[idx], self.y[idx]]

    # get indexes for train and test rows
    def get_splits(self, n_test=0.33):
        # determine sizes
        test_size = round(n_test * len(self.X))
        train_size = len(self.X) - test_size
        # calculate the split
        return random_split(self, [train_size, test_size])


class MLP(Module):
    # define model elements
    def __init__(self, numFeatures, numTargets, numFirstLayerNeurons, alpha):
        super(MLP, self).__init__()
        numNeurons1 = numFirstLayerNeurons
        numNeurons2 = math.ceil(numFirstLayerNeurons * alpha)
        numNeurons3 = math.ceil(numFirstLayerNeurons * alpha ** 2)
        # input to first hidden layer
        self.hidden1 = Linear(numFeatures, numNeurons1)
        xavier_uniform_(self.hidden1.weight)
        self.act1 = Sigmoid()
        # second hidden layer
        self.hidden2 = Linear(numNeurons1, numNeurons2)
        xavier_uniform_(self.hidden2.weight)
        self.act2 = Sigmoid()
        # third hidden layer
        self.hidden3 = Linear(numNeurons2, numNeurons3)
        xavier_uniform_(self.hidden2.weight)
        self.act3 = Sigmoid()
        # fourth hidden layer and output
        self.hidden4 = Linear(numNeurons3, numTargets)
        xavier_uniform_(self.hidden3.weight)

    # forward propagate input
    def forward(self, X):
        # input to first hidden layer
        X = self.hidden1(X)
        X = self.act1(X)
        # second hidden layer
        X = self.hidden2(X)
        X = self.act2(X)
        # third hidden layer
        X = self.hidden3(X)
        X = self.act3(X)
        # third hidden layer and output
        X = self.hidden4(X)
        return X


# prepare the dataset
def prepare_data(path, numFeatures, numTargets, batch_size, deviceName):
    # load the dataset
    dataset = CSVDataset(path, numFeatures, numTargets, deviceName, normalize=True)
    # calculate split
    trainData, testData = dataset.get_splits()
    # prepare data loaders
    train_dl = DataLoader(trainData, batch_size=batch_size, shuffle=False, pin_memory=False)
    test_dl = DataLoader(testData, batch_size=batch_size, shuffle=False, pin_memory=False)
    return train_dl, test_dl, testData


# train the model
def train_model(train_dl, model, lr, numEpochs, optimizerFlag, modelSaveFilePath, startEpoch):
    # define the optimization
    criterion = MSELoss()
    if optimizerFlag[0] == 'A':
        optimizer = Adam(model.parameters(), lr=lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0, amsgrad=False)
    if optimizerFlag[0] == 'S':
        optimizer = SGD(model.parameters(), lr=lr, momentum=float(optimizerFlag[1]))
    # enumerate epochs
    for epoch in range(startEpoch + 1, numEpochs + 1):
        # enumerate mini batches
        loop = tqdm(enumerate(train_dl), total=len(train_dl), leave=False)
        for i, (inputs, targets) in loop:
            # clear the gradients
            optimizer.zero_grad()
            # compute the model output
            yhat = model(inputs)
            # calculate loss
            loss = criterion(yhat, targets)
            # credit assignment
            loss.backward()
            # update model weights
            optimizer.step()
            # upgrade progress bar
            loop.set_description(f"Epoch [{epoch}/{numEpochs}]")
            # loop.set_postfix(loss=loss.item(),acc=torch.rand(1).item())
        if epoch % 10 == 0:
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'lr': lr,
            }, modelSaveFilePath + str(epoch) + ".pt")


# evaluate the model
def evaluate_model(test_dl, model, numTargets):
    predictions, actuals = list(), list()
    for i, (inputs, targets) in enumerate(test_dl):
        # evaluate the model on the test set
        yhat = model(inputs)
        # retrieve numpy array
        yhatcpu = yhat.cpu()
        yhat = yhatcpu.detach().numpy()
        targetscpu = targets.cpu()
        actual = targetscpu.numpy()
        actual = actual.reshape((len(actual), numTargets))
        # store
        predictions.append(yhat)
        actuals.append(actual)
    predictions, actuals = vstack(predictions), vstack(actuals)
    # calculate mse
    mse = mean_squared_error(actuals, predictions)
    return mse


# make a class prediction for one numPredictions rows of data
def predict(testData, model, numPredictions, modelSaveFileName, main_mse):
    gs = mpl.gridspec.GridSpec(math.ceil(math.sqrt(numPredictions)), math.ceil(math.sqrt(numPredictions)))
    fig = plt.figure()
    fig.set_size_inches(18.5, 10.5)
    for i in range(numPredictions):
        testIndex = random.randrange(0, len(testData))
        testrow = testData[testIndex]
        # make prediction
        yhat = model(testrow[0])
        yhat = yhat.cpu().detach().numpy()
        real = testrow[1].cpu().detach().numpy()
        print('Predicted:\n', yhat, '\nreal:\n', real)
        mse = mean_squared_error(yhat, real)
        xline = [1, 2, 3, 4, 5, 6]
        ax = fig.add_subplot(gs[i])
        ax.scatter(xline, real, color="blue", marker='o', s=80)
        ax.scatter(xline, yhat, color="red", marker="x", s=80)
        ax.set_title("testIndex: " + str(testIndex) + "; loss: " + str(mse))
    fig.suptitle(modelSaveFileName + "\nmain_mse: " + str(main_mse) + "\nreal = red; pred = blue;", fontsize=16)
    plt.savefig("Tests_der_Modelle//" + modelSaveFileName + '.png')
    return yhat


def getModelSaveFilePath(batch_size, numFirstLayerNeurons, alpha, optimizerFlag, lr, numEpochs, epochFlag):
    modelSaveFilePath = "Modelle\\B" + str(batch_size) + "-N" + str(numFirstLayerNeurons) + "-A" + str(alpha) + "-O" + optimizerFlag[0] + optimizerFlag[1] + "-LR" + str(lr)
    if not os.path.exists(modelSaveFilePath):
        os.makedirs(modelSaveFilePath)
    if epochFlag == 0:
        modelSaveFilePath = modelSaveFilePath + "\\""B" + str(batch_size) + "-N" + str(numFirstLayerNeurons) + "-A" + str(alpha) + "-O" + optimizerFlag[0] + optimizerFlag[1] + "-LR" + str(
            lr) + "-E" + str(
            numEpochs) + ".pt"
    if epochFlag == 1:
        modelSaveFilePath = modelSaveFilePath + "\\""B" + str(batch_size) + "-N" + str(numFirstLayerNeurons) + "-A" + str(alpha) + "-O" + optimizerFlag[0] + optimizerFlag[1] + "-LR" + str(lr) + "-E"
    return modelSaveFilePath


# Start Programm
torch.manual_seed(0)
random.seed(0)
path = 'Datasets\\XFEL_KW0_Results_2.csv'
deviceName = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
numFeatures = 8
numTargets = 6
batch_size = 8
numFirstLayerNeurons = 500
alpha = 0.5
lr = 0.001
startEpoch = 0
numEpochs = 10 * 10
optimizerFlag = ['A', '']
numPredictions = 9

# prepare the data
train_dl, test_dl, testData = prepare_data(path, numFeatures, numTargets, batch_size, deviceName)
# define the network
model = MLP(numFeatures, numTargets, numFirstLayerNeurons, alpha)
model = model.to(deviceName)
# modelSaveFilePath = getModelSaveFilePath(batch_size, numFirstLayerNeurons, alpha, optimizerFlag, lr, numEpochs, 0)
# load the model
for epoch10 in range(numEpochs, 0, -10):
    modelSaveFilePath = getModelSaveFilePath(batch_size, numFirstLayerNeurons, alpha, optimizerFlag, lr, epoch10, 0)
    if os.path.isfile(modelSaveFilePath):
        checkpoint = torch.load(modelSaveFilePath)
        startEpoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()
        break
# train the model
modelSaveFilePath = getModelSaveFilePath(batch_size, numFirstLayerNeurons, alpha, optimizerFlag, lr, epoch10, 1)
train_model(train_dl, model, lr, numEpochs, optimizerFlag, modelSaveFilePath, startEpoch)
# evaluate the model
mse = evaluate_model(test_dl, model, numTargets)
print('MSE: %f, RMSE: %.3f' % (mse, sqrt(mse)))
# make a single prediction
modelSaveFileName = "B" + str(batch_size) + "-N" + str(numFirstLayerNeurons) + "-A" + str(alpha) + "-O" + optimizerFlag[0] + optimizerFlag[1] + "-LR" + str(lr) + "-E" + str(numEpochs) + ".pt"
predict(testData, model, numPredictions, modelSaveFileName, mse)

# START: Plotte dataset
# datasetShape = dataset.shape
# divider = 1000
# filler = np.arange(math.ceil(datasetShape[0] / divider))
# dataset = np.array_split(dataset, divider)
# # print(dataset)
# # print(filler.shape)
# # print(dataset[0][0].shape)
# # print(datasetShape, filler)
# numfeatures = datasetShape[1]
# gs = mpl.gridspec.GridSpec(math.ceil(math.sqrt(numfeatures)), math.ceil(math.sqrt(numfeatures)))
# fig = plt.figure()
# for i in range(numfeatures):
#     ax = fig.add_subplot(gs[i])
#     ax.scatter(filler, dataset[0][i])
#     # ax.tight_layout()
# plt.show()
# ENDE: Plotte dataset
