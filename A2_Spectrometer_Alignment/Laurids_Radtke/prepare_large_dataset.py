import h5py
import numpy as np
from numpy.core.fromnumeric import shape
import torch
from tqdm import tqdm
import gzip
import shutil
import os

#TODO add noise and offset

def compress_tensor(amount_chunks): #chunk_size = wie groß ein Teil Tensor sein soll (am besten 10er potenzen)
    filename ='export1M.h5'

    x = []
    y = []

    print("getting data...")
    with h5py.File(filename, 'r') as f:
        for chunk_no in range(int(shape(f)[0]/amount_chunks)):
            print("chunk no.:"+str(chunk_no)+"/"+str(int(shape(f)[0]/amount_chunks)))
            for key in tqdm(range(chunk_no*amount_chunks,amount_chunks+chunk_no*amount_chunks)):
                image = np.array(f["/"+str(key)+"/X"])
                target = np.array(f["/"+str(key)+"/Y"])
                if np.amax(image) > 0:
                    x.append(image)
                    y.append(target)

            print("putting data into tensor...")     
            X = torch.tensor(x, dtype=torch.float32)  
            Y = torch.tensor(y, dtype=torch.float32) 

            t_name = ["xtensor"+str(chunk_no)+".pt","ytensor"+str(chunk_no)+".pt"]

            for name in t_name:
                if(name[0] == x):
                    torch.save(X, name)
                else: 
                    torch.save(Y, name)
                print("compressing tensor...")
                with open(name, 'rb') as f_in:
                    with gzip.open(name+".gz", 'wb') as f_out:
                        shutil.copyfileobj(f_in,f_out)

                os.remove(name)

def read_tensor(tensor_num):
    with gzip.open("xtensor"+str(tensor_num)+".pt", 'rb') as fl:
        X = fl.read()
    
    with gzip.open("ytensor"+str(tensor_num)+".pt", 'rb') as fl:
        Y = fl.read()
    
    return X,Y
            
#compress_tensor(100000)



