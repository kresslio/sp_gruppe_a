import h5py 
from math import ceil
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import MSELoss
from sklearn.preprocessing import MinMaxScaler
from torch.utils.data import Dataset, DataLoader, random_split
from torch.optim import SGD, Adam
from tqdm import tqdm
from numpy import vstack, sqrt
from sklearn.metrics import mean_squared_error
import os
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from PIL import Image

# num_filter0 = 4
# num_filter1 = num_filter0 * 2
# num_filter2 = num_filter1 * 2
# conv_size0 = 7
# conv_size1 = 5
# conv_size2 = 3
# conv_stride0 = 2
# conv_stride1 = 2
# conv_stride2 = 2
# pool_size = 2
# pool_stride = 2
# alpha = 0.5
# num_neurons0 = 500
# num_features = 40500
# num_targets = 3


class HDF5Dataset(Dataset):
    def __init__(self, path, device, normalize):
        sims = 100000 
        self.x = []
        self.y = []
        print('reading file...')
        with h5py.File(path, 'r') as f:
            for key in range(sims):
                X = np.array(f['/' + str(key) + '/X'])
                Y = np.array(f['/' + str(key) + '/Y'])
                if(normalize):
                    X = MinMaxScaler().fit_transform(X)

                if np.amax(X) > 0:  # ignore the blank images (no data)
                    self.x.append(X)
                    self.y.append(Y)
        print('putting data into tensor...')
        self.x = torch.tensor(self.x, dtype=torch.float32, device=device)  # input
        self.x = self.x.unsqueeze(1)
        self.y = torch.tensor(self.y, dtype=torch.float32, device=device)  # input

        self.len = len(self.x)

    # number of rows in the dataset
    def __len__(self):
        return self.len

    # get a row at an index
    def __getitem__(self, index):
        self.xc = self.x[index]
        self.yc = self.y[index]
        return self.xc, self.yc

    #clips testdata from end of tensor
    def get_splits(self, test_data_size): 
        train_data_size = len(self) - test_data_size

        '''
        train_x = self.x[:-test_data_size]
        test_x = self.x[-test_data_size:]
        train_y = self.y[:-test_data_size]
        test_y = self.y[-test_data_size:]
        train_data = (train_x, train_y)
        test_data = (test_x, test_y)
        print(train_data[0][:])
        return train_data, test_data
        '''
        return random_split(self, [train_data_size, test_data_size])

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1,8,7,2)
        self.conv2 = nn.Conv2d(8,16,5,2)
        self.conv3 = nn.Conv2d(16,32,3,2)
        self.fc1 = nn.Linear(128, 32)
        self.fc2 = nn.Linear(32, 3)
    
    def forward(self, x):
        x=self.conv1(x)
        x=F.relu(x)
        x=F.max_pool2d(x,2)
        x=self.conv2(x)
        x=F.relu(x)
        x=F.max_pool2d(x,2)
        x=self.conv3(x)
        x=F.relu(x)
        x=F.max_pool2d(x,2)
        #print(x.shape)
        x = torch.flatten(x, 1)
        #print(x.shape)
        x = self.fc1(x)
        #print(x.shape())
        x = F.relu(x)
        x = self.fc2(x)
        #print(x.shape())
        output = x
        #output = F.log_softmax(x, dim=1)
        return output

def prepare_data(path, device, batch_size,test_data_size):
    dataset = HDF5Dataset(path, device, normalize=True)
    train_data, test_data = dataset.get_splits(test_data_size)
    print('preparing DataLoader...')
    train_dl = DataLoader(train_data, batch_size=batch_size, shuffle=True, pin_memory=False)
    test_dl = DataLoader(test_data, batch_size=batch_size, shuffle=False, pin_memory=False)
    return train_dl, test_dl, test_data

def train_model(model,train_dl,learning_rate,num_epochs,modelSaveFilePath,start_epoch):
    criterion = MSELoss()
    optimizer = Adam(model.parameters(), lr=learning_rate, betas=(0.9, 0.999), eps=1e-08, weight_decay=0, amsgrad=False)
    print('starting training...')
    for epoch in range(start_epoch + 1, num_epochs + 1):
        loop = tqdm(enumerate(train_dl), total=len(train_dl), leave=False)
        for i, (inputs, targets) in loop:
            # clear the gradients
            optimizer.zero_grad()
            # compute the model output
            yhat = model(inputs)
            # calculate loss
            loss = criterion(yhat, targets)
            # credit assignment
            loss.backward()
            # update model weights
            optimizer.step()
            # upgrade progress bar
            loop.set_description("Epoch [{0}/{1}]".format(epoch, num_epochs))
        if epoch % 10 == 0:
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'lr': learning_rate,
            }, modelSaveFilePath + str(epoch) + ".pt")

def evaluate_model(test_dl, model,plot = None):
    predictions, actuals = list(), list()
    for i, (inputs, targets) in enumerate(test_dl):
        # evaluate the model on the test set
        yhat = model(inputs)
        # retrieve numpy array
        yhatcpu = yhat.cpu()
        yhat = yhatcpu.detach().numpy()
        targetscpu = targets.cpu()
        actual = targetscpu.numpy()
        actual = actual.reshape((len(actual), 3))
        # store
        predictions.append(yhat)
        actuals.append(actual)
    predictions, actuals = vstack(predictions), vstack(actuals)
    
    if plot is not None:
        img = plot_pred(predictions, actuals, "primitive network results")
        Image.fromarray(img).save(plot)

    # calculate mse
    mse = mean_squared_error(actuals, predictions)
    return mse

def plot_pred(predictions, targets, title=None):
    plt.rc('font', size=15)
    plt.rc('axes', titlesize=15)
    plt.rc('axes', labelsize=17)
    plt.rc('xtick', labelsize=15)
    plt.rc('ytick', labelsize=15)
    plt.rc('figure', titlesize=20)
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharex="all", sharey="all")
    fig.suptitle(title)
    fig.set_size_inches(20, 12)
    kwargs = dict(
        xlabel="predictions",
        xlim=[-5.0, 5.0],
        ylim=[-5.0, 5.0],
        xticks=[-5.0, 0.0, 5.0],
        yticks=[-5.0, 0.0, 5.0],
        aspect="equal",
        adjustable="box",
    )
    make_subplot(ax1, predictions, targets, 0, title="x", ylabel="targets", **kwargs)
    make_subplot(ax2, predictions, targets, 1, title="y", **kwargs)
    make_subplot(ax3, predictions, targets, 2, title="z", **kwargs)
    plt.tight_layout()
    canvas = FigureCanvas(fig)
    canvas.draw()
    width, height = fig.get_size_inches() * fig.get_dpi()
    img = np.fromstring(canvas.tostring_rgb(), dtype="uint8").reshape((int(height), int(width), 3))
    # plt.show()
    plt.close()
    return img

def make_subplot(ax, predictions, targets, col, **kwargs):
    ax.scatter(predictions[:, col], targets[:, col], s=1, color="b", marker=",")
    ax.set(**kwargs)
    ax.grid()
    ax.margins(0)

def getModelSaveFilePath(batch_size, learning_rate, num_epochs, epoch_flag):
    modelSaveFilePath = "Modelle\\B" + str(batch_size) + "-LR" + str(learning_rate)
    if not os.path.exists(modelSaveFilePath):
        os.makedirs(modelSaveFilePath)
    if epoch_flag == 0:
        modelSaveFilePath = modelSaveFilePath + "\\""B" + str(batch_size) + "-LR" + str(learning_rate) + "-E" + str(num_epochs) + ".pt"
    if epoch_flag == 1:
        modelSaveFilePath = modelSaveFilePath + "\\""B" + str(batch_size) + "-LR" + str(learning_rate) + "-E"
    return modelSaveFilePath

def get_device(device_type=None):
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    if device_type == "cpu":
        device = torch.device("cpu")
    print("device: {%s}" % device.type)
    return device


path = "training_data_alignment.h5"
plot_path = "plot.png"
device = get_device()
batch_size = 8
start_epoch = 1
learning_rate = 0.0001
test_data_size = 1000
num_epochs = 300

train_dl, test_dl, test_data = prepare_data(path, device, batch_size, test_data_size) 

model = Net()
model = model.to(device)
modelSaveFilePath = getModelSaveFilePath(batch_size, learning_rate, num_epochs, 0)
print("geht los")
for epoch10 in range(num_epochs, 0, -10):
    modelSaveFilePath = getModelSaveFilePath(batch_size, learning_rate, epoch10, 0)
    if os.path.isfile(modelSaveFilePath):
        # print(modelSaveFilePath)
        # print("^")
        checkpoint = torch.load(modelSaveFilePath)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['model_state_dict'])
        model.eval()
        break;

modelSaveFilePath = getModelSaveFilePath(batch_size, learning_rate, epoch10, 1)

train_model(model,train_dl,learning_rate,num_epochs,modelSaveFilePath,start_epoch)
print('done training')

mse = evaluate_model(test_dl, model,plot=plot_path)
f = open("mse.txt","w")
f.write(str(mse))
f.close()
print('MSE: %f, RMSE: %.3f' % (mse, sqrt(mse)))
  
#eine epoche sind alle daten
#eine epoche besteht aus mehreren batches
