import h5py
import os
import numpy as np
import torch
from sklearn.preprocessing import MinMaxScaler
from numpy.core.fromnumeric import shape

# VERALTET
# def cleared_export_test():
#     with h5py.File("cleared_export.h5", "r") as f:
#         print(shape(f)[0])
#         f_list = list(f.keys())
#         for key in range(shape(f)[0]):
#             print(f_list[key])
#             print(f["/" + f_list[key] + "/X"])
#
#
# def delete_cleared_export():
#     if os.path.isfile("cleared_export.h5"):
#         os.remove("cleared_export.h5")


def clear_export():
    if not os.path.isfile("x-tensor.pt") or not os.path.isfile("y-tensor.pt"):
        x = []
        y = []
        with h5py.File("export.h5", "r") as f:
            for key in range(shape(f)[0]):
                X = np.array(f["/" + str(key) + "/X"])
                Y = np.array(f["/" + str(key) + "/Y"])
                X = MinMaxScaler().fit_transform(X)  # normalise the data between 0 and 1
                if np.amax(X) > 0:
                    x.append(X)
                    y.append(Y)
            x = torch.tensor(x, dtype=torch.float32)  # input
            x = x.unsqueeze(1)
            y = torch.tensor(y, dtype=torch.float32)  # input
        torch.save(x, 'x-tensor.pt')
        torch.save(y, 'y-tensor.pt')

# delete_cleared_export()
# clear_export()
# cleared_export_test()
