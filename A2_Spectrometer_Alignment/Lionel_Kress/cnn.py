from math import ceil
import math
import h5py
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import MSELoss
from sklearn.preprocessing import MinMaxScaler
from torch.utils.data import Dataset, DataLoader, random_split
from torch.optim import SGD, Adam
from tqdm import tqdm
from numpy import vstack, sqrt, shape
from sklearn.metrics import mean_squared_error
import os
import matplotlib.pyplot as plt
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from PIL import Image
import clear_h5_file as h5clear
import matplotlib.pyplot as plt
import matplotlib as mpl
import random

# ___________________________________________________________________________
# for debugging
import inspect


def PrintFrame():
    callerframerecord = inspect.stack()[1]  # 0 represents this line
    # 1 represents line at caller
    frame = callerframerecord[0]
    info = inspect.getframeinfo(frame)
    print(info.function, info.lineno, flush=True)


# ___________________________________________________________________________


# TODO dynamische Layeranzahl
# TODO Modell-Namenbennenung umgestalten
# TODO Modelle-Ordnererstellung für Linux und Windows kompatibel machen

torch.manual_seed(0)
random.seed(0)
path_parent = os.path.dirname(os.getcwd())
os.chdir(path_parent)
# h5clear.cleared_export_test()
PrintFrame()
h5clear.clear_export()
cleared_export_path = "cleared_export.h5"
plot_path = "plot.png"
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print("device:", device, torch.cuda.get_device_name(0))
test_data_size = 1000
num_predictions = 9
batch_size = 4
learning_rate = 0.00001
num_epochs = 1000
# nn.Sequential variables:
num_filter0 = 16
num_filter1 = num_filter0 * 2
num_filter2 = num_filter1 * 2
conv_size0 = 7
conv_size1 = conv_size0 - 2
conv_size2 = conv_size1 - 2
conv_stride0 = 2
conv_stride1 = 2
conv_stride2 = 2
pool_size = 2
pool_stride = 2
alpha = 0.5
num_neurons0 = 500
num_neurons1 = ceil(num_neurons0 * alpha)
num_neurons2 = ceil(num_neurons1 * alpha)
# conv2d_Out0 = ((200 - conv_size0 + 2 * (conv_size0 - conv_stride0)) / conv_stride0 + 1) / pool_size
# conv2d_Out1 = ((conv2d_Out0 - conv_size1 + 2 * (conv_size1 - conv_stride1)) / conv_stride1 + 1) / pool_size
# conv2d_Out2 = ((conv2d_Out1 - conv_size2 + 2 * (conv_size2 - conv_stride2)) / conv_stride2 + 1) / pool_size
# num_features = ceil(conv2d_Out2 * conv2d_Out2 * num_filter2)
num_features = 576
num_targets = 3


class TensorDataset(Dataset):
    def __init__(self, normalize):
        self.x = torch.load('x-tensor.pt')
        self.y = torch.load('y-tensor.pt')
        self.len = len(self.x)

    # number of rows in the dataset
    def __len__(self):
        return self.len

    # get a row at an index
    def __getitem__(self, index):
        self.xc = self.x[index]
        self.yc = self.y[index]
        return self.xc, self.yc

    # clips testdata from end of tensor
    def get_splits(self):
        train_data_size = len(self) - test_data_size
        return random_split(self, [train_data_size, test_data_size])


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.features = nn.Sequential(
            nn.Conv2d(1, num_filter0, kernel_size=conv_size0, stride=conv_stride0, padding=conv_size0 - conv_stride0),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool_size, stride=pool_stride),
            nn.Conv2d(num_filter0, num_filter1, kernel_size=conv_size1, stride=conv_stride1, padding=conv_size1 - conv_stride1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool_size, stride=pool_stride),
            nn.Conv2d(num_filter1, num_filter2, kernel_size=conv_size2, stride=conv_stride2, padding=conv_size2 - conv_stride2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=pool_size, stride=pool_stride),
        )

        self.classifier = nn.Sequential(nn.Linear(num_features, num_neurons0), nn.ReLU(inplace=True), nn.Linear(num_neurons0, num_neurons1), nn.ReLU(inplace=True),
                                        nn.Linear(num_neurons1, num_targets))

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), -1)
        # print(shape(x))
        x = self.classifier(x)
        return x


def prepare_data():
    PrintFrame()
    dataset = TensorDataset(normalize=True)
    PrintFrame()
    train_data, test_data = dataset.get_splits()
    print("preparing DataLoader...", end="\r", flush=True)
    train_dl = DataLoader(train_data, batch_size=batch_size, shuffle=True, pin_memory=True)
    test_dl = DataLoader(test_data, batch_size=batch_size, shuffle=False, pin_memory=True)
    return train_dl, test_dl, test_data


def train_model(model, train_dl, modelSaveFilePath, start_epoch):
    criterion = MSELoss()
    optimizer = Adam(model.parameters(), lr=learning_rate, betas=(0.9, 0.999), eps=1e-08, weight_decay=0, amsgrad=False)
    for epoch in range(start_epoch + 1, num_epochs + 1):
        loop = tqdm(enumerate(train_dl), total=len(train_dl), leave=False)
        for i, (inputs, targets) in loop:
            inputs, targets = inputs.to(device), targets.to(device)
            # clear the gradients
            optimizer.zero_grad()
            # compute the model output
            yhat = model(inputs)
            # calculate loss
            loss = criterion(yhat, targets)
            # credit assignment
            loss.backward()
            # update model weights
            optimizer.step()
            # upgrade progress bar
            loop.set_description(f"Epoch [{epoch}/{num_epochs}]")
        if epoch % 10 == 0 and epoch != 0:
            torch.save({"epoch": epoch, "model_state_dict": model.state_dict(), "optimizer_state_dict": optimizer.state_dict(), "lr": learning_rate, }, modelSaveFilePath + str(epoch) + ".pt")


def evaluate_model(test_dl, model):
    predictions, actuals = list(), list()
    for i, (inputs, targets) in enumerate(test_dl):
        inputs, targets = inputs.to(device), targets.to(device)
        # evaluate the model on the test set
        yhat = model(inputs)
        # retrieve numpy array
        yhatcpu = yhat.cpu()
        yhat = yhatcpu.detach().numpy()
        targetscpu = targets.cpu()
        actual = targetscpu.numpy()
        actual = actual.reshape((len(actual), 3))
        # store
        predictions.append(yhat)
        actuals.append(actual)
    predictions, actuals = vstack(predictions), vstack(actuals)
    # calculate mse
    mse = mean_squared_error(actuals, predictions)
    return mse


# make a class prediction for one numPredictions rows of data
def predict(test_data, model, modelSaveFileName, main_mse):
    gs = mpl.gridspec.GridSpec(math.ceil(math.sqrt(num_predictions)), math.ceil(math.sqrt(num_predictions)))
    fig = plt.figure()
    fig.set_size_inches(18.5, 10.5)
    for i in range(num_predictions):
        test_index = random.randrange(0, len(test_data))
        test_row = test_data[test_index]
        test_row_0 = test_row[0].unsqueeze(1).to(device)
        # make prediction
        yhat = model(test_row_0)
        yhat = yhat.cpu().detach().numpy()
        print(shape(test_row[1]))
        real = test_row[1].reshape(1, 3).cpu().detach().numpy()
        print('Predicted:\n', yhat, '\nreal:\n', real)
        mse = mean_squared_error(yhat, real)
        xline = [1, 2, 3]
        ax = fig.add_subplot(gs[i])
        ax.scatter(xline, real, color="blue", marker='o', s=80)
        ax.scatter(xline, yhat, color="red", marker="x", s=80)
        ax.set_title("testIndex: " + str(test_index) + "; loss: " + str(mse))
    fig.suptitle(modelSaveFileName + "\nmain_mse: " + str(main_mse) + "\nreal = red; pred = blue;", fontsize=16)
    plt.savefig("Prediction-Plots//" + modelSaveFileName + '.png')
    return yhat

def getModelSaveFilePath(num_epochs, epoch_flag):
    modelSaveFilePath = "Modelle//N" + str(num_neurons0) + "-A" + str(alpha) + "-F" + str(num_filter0) + "-CS" + str(conv_size0) + "-B" + str(batch_size) + "-LR" + str(learning_rate)
    if not os.path.exists(modelSaveFilePath):
        os.makedirs(modelSaveFilePath)
    if epoch_flag == 0:
        modelSaveFilePath = modelSaveFilePath + "//N" + str(num_neurons0) + "-A" + str(alpha) + "-F" + str(num_filter0) + "-CS" + str(conv_size0) + "-B" + str(batch_size) + "-LR" + str(
            learning_rate) + "-E" + str(num_epochs) + ".pt"
    if epoch_flag == 1:
        modelSaveFilePath = modelSaveFilePath + "//N" + str(num_neurons0) + "-A" + str(alpha) + "-F" + str(num_filter0) + "-CS" + str(conv_size0) + "-B" + str(batch_size) + "-LR" + str(
            learning_rate) + "-E"
    return modelSaveFilePath


train_dl, test_dl, test_data = prepare_data()


model = Net()
model = model.to(device)
modelSaveFilePath = getModelSaveFilePath(num_epochs, 0)

for epoch10 in range(num_epochs, 0, -10):
    modelSaveFilePath = getModelSaveFilePath(epoch10, 0)
    if os.path.isfile(modelSaveFilePath):
        checkpoint = torch.load(modelSaveFilePath, map_location='cuda:0')
        start_epoch = checkpoint["epoch"]
        model.load_state_dict(checkpoint["model_state_dict"])
        model.eval()
        break

modelSaveFilePath = getModelSaveFilePath(epoch10, 1)

train_model(model, train_dl, modelSaveFilePath, start_epoch)
print("done training", end="\r", flush=True)

mse = evaluate_model(test_dl, model)
print("MSE: %f, RMSE: %.3f" % (mse, sqrt(mse)))

# make a single prediction
if not os.path.exists("Prediction-Plots"):
    os.makedirs("Prediction-Plots")
modelSaveFileName = "N" + str(num_neurons0) + "-A" + str(alpha) + "-F" + str(num_filter0) + "-CS" + str(conv_size0) + "-B" + str(batch_size) + "-LR" + str(
            learning_rate) + "-E" + str(num_epochs) + ".pt"
predict(test_data, model, modelSaveFileName, mse)
